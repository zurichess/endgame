// Copyright 2014-2017 The Zurichess Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package endgame

import (
	. "bitbucket.org/zurichess/zurichess/board"
	"log"
	"time"
)

// Outcome encodes the outcome of the game.
type Outcome int8

const (
	// Unknown means the position result is unknown
	Unknown Outcome = iota
	// Lost means the current player loses in this positions.
	Lost
	// Draw means position is drawn.
	Draw
	// Won means the current player wins in this positions.
	Won
	// Invalid means the position is invalid (e.g., player to move can capture the king).
	Invalid
)

type Result struct {
	Outcome Outcome
	DTZ     int16
}

// Convert the result to current player's POV.
func (r Result) Reverse() Result {
	switch r.Outcome {
	case Won:
		r.Outcome = Lost
		r.DTZ++
	case Lost:
		r.Outcome = Won
		r.DTZ++
	}
	if r.DTZ >= 100 {
		r.Outcome = Draw
		r.DTZ = 0
	}
	return r
}

// minimax provides functionality for implementing the minimax update.
type minimax struct {
	NumMoves int
	Result   Result
}

func (mm *minimax) update(r Result) {
	if r.Outcome == Invalid {
		return
	}

	r = r.Reverse()

	if mm.NumMoves++; mm.NumMoves == 1 {
		mm.Result = r
		return
	}
	if mm.Result.Outcome == Won && (r.Outcome == Unknown || r.Outcome == Draw || r.Outcome == Lost) {
		return
	}
	// Do not declare lost positions if the opponent cannot win all positions.
	if mm.Result.Outcome == Lost && r.Outcome == Unknown || mm.Result.Outcome == Unknown && r.Outcome == Lost {
		mm.Result = Result{Unknown, 0}
		return
	}

	// Find the maximum.
	if mm.Result.Outcome < r.Outcome {
		mm.Result = r
		return
	}
	if mm.Result.Outcome > r.Outcome {
		return
	}

	// Return the longest path for lost positions.
	if mm.Result.Outcome == Lost {
		if mm.Result.DTZ < r.DTZ {
			mm.Result = r
			return
		}
	}
	if mm.Result.Outcome == Won {
		if mm.Result.DTZ > r.DTZ {
			mm.Result = r
			return
		}
	}
}

// distance[i][j] is the minimum number of moves needed by a King to reach i from j.
var distance [SquareArraySize][SquareArraySize]int32

// Evaluate evaluates an endgame.
// Returns (Unknown, 0) when the endgame is not known.
// Returns (Win/Draw/Loss, number of moves) otherwise.
func Evaluate(pos *Position) Result {
	all := pos.ByColor(White) | pos.ByColor(Black)
	num := all.Count()

	switch num {
	case 2:
		return Result{Draw, 0} // Theoretical draw.
	case 3:
		if pos.ByFigure(Pawn) != 0 {
			return KPK(pos)
		}
		if pos.ByFigure(Knight) != 0 {
			return Result{Draw, 0} // Theoretical draw.
		}
		if pos.ByFigure(Bishop) != 0 {
			return Result{Draw, 0} // Theoretical draw.
		}
		if pos.ByFigure(Rook) != 0 {
			return KRK(pos)
		}
		if pos.ByFigure(Queen) != 0 {
			return KQK(pos)
		}
	case 4:
		if pos.ByPiece(White, Rook) != 0 && pos.ByPiece(Black, Rook) != 0 {
			return KRKR(pos)
		}
	}
	return Result{Unknown, 0}
}

// max returns maximum of a and b.
func max(a, b int32) int32 {
	if a >= b {
		return a
	}
	return b
}

// run runs f and returns the elapsed time.
func run(f func()) time.Duration {
	start := time.Now()
	f()
	return time.Now().Sub(start)
}

// init initializes distances.
func init() {
	for i := SquareMinValue; i <= SquareMaxValue; i++ {
		for j := SquareMinValue; j <= SquareMaxValue; j++ {
			f, r := int32(i.File()-j.File()), int32(i.Rank()-j.Rank())
			f, r = max(f, -f), max(r, -r) // absolute value
			distance[i][j] = max(f, r)
		}
	}

	log.Println("krk took", run(krkBuild))
	log.Println("kqk took", run(kqkBuild))
	//log.Println("kpk took", run(kpkBuild))
	// log.Println("krkr took", run(krkrBuild))
	//log.Println("krpkr took", run(krpkrBuild))
}

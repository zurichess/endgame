// Copyright 2014-2017 The Zurichess Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package endgame

import (
	. "bitbucket.org/zurichess/zurichess/board"
	"log"
)

// WDL and DTM (DTZ?) bitbases.
var kqk []Result

func kqkBuild() {
	log.Println("building kqk")
	pieces := pieces{WhiteKing, WhiteQueen, BlackKing}
	kqk = buildTable(pieces)
}

// KQK evaluates the KQK endgame.
func KQK(pos *Position) Result {
	var wk, wq, bk Square
	var toMove int

	if pos.ByPiece(pos.Us(), Queen) != 0 {
		wk = pos.ByPiece(pos.Us(), King).AsSquare()
		wq = pos.ByPiece(pos.Us(), Queen).AsSquare()
		bk = pos.ByPiece(pos.Them(), King).AsSquare()
		toMove = 0
	} else if pos.ByPiece(pos.Them(), Queen) != 0 {
		wk = pos.ByPiece(pos.Them(), King).AsSquare()
		wq = pos.ByPiece(pos.Them(), Queen).AsSquare()
		bk = pos.ByPiece(pos.Us(), King).AsSquare()
		toMove = 1
	} else {
		panic("no rooks")
	}

	return kqk[encode(squares{wk, wq, bk}, toMove)]
}

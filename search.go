// Copyright 2014-2017 The Zurichess Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package endgame

import (
	. "bitbucket.org/zurichess/zurichess/board"
	"log"
)

// pieces stores the endgame configuration.
// First piece is WhiteKing.
// First pieces are Whites.
type pieces [8]Piece

// squares stores piece positions.
// For endgames with fewer pieces SquareA1 (0) is stored.
type squares [8]Square

type searcher struct {
	pieces pieces
	table  []Result

	instack  []bool
	stack    []squares
	stacknew []squares
}

func buildTable(pis pieces) []Result {
	size := getTableSize(pis)
	log.Println("allocating tables of size", size)
	searcher := &searcher{
		pieces:  pis,
		table:   make([]Result, size),
		instack: make([]bool, size),
	}
	searcher.build()
	return searcher.table
}

func (s *searcher) build() {
	for s1 := SquareMinValue; s1 <= SquareMaxValue; s1++ {
		for s2 := SquareMinValue; s2 <= SquareMaxValue; s2++ {
			for s3 := SquareMinValue; s3 <= SquareMaxValue; s3++ {
				sqs := squares{s1, s2, s3}
				s.buildOne(sqs)
			}
		}
	}

	s.stack, s.stacknew = s.stacknew, s.stack

	for len(s.stack) > 0 {
		sqs := s.stack[len(s.stack)-1]
		s.stack = s.stack[:len(s.stack)-1]
		if len(s.stack) == 0 {
			log.Println("processing", len(s.stacknew))
			s.stack, s.stacknew = s.stacknew, s.stack
		}

		s.instack[encode(sqs, 0)] = false
		s.buildOne(sqs)
	}
}

func (s *searcher) buildOne(sqs squares) {
	// Comppute various boards.
	var all Bitboard       // all pieces
	var kings [2]Square    // location of the two kings
	var boards [2]Bitboard // location of figures of each color
	for i := range s.pieces {
		if s.pieces[i] == WhiteKing {
			kings[0] = sqs[i]
		}
		if s.pieces[i] == BlackKing {
			kings[1] = sqs[i]
		}
		if s.pieces[i].Color() == White {
			boards[0] |= sqs[i].Bitboard()
		}
		if s.pieces[i].Color() == Black {
			boards[1] |= sqs[i].Bitboard()
		}
		if s.pieces[i] != NoPiece {
			all |= sqs[i].Bitboard()
		}
	}

	// Compute the attack boards to avoid moving the king into check.
	var attack [2]Bitboard
	for i := range s.pieces {
		switch s.pieces[i] {
		case WhitePawn:
			bb := sqs[i].Bitboard()
			attack[0] |= North(East(bb) | West(bb))
		case WhiteRook:
			attack[0] |= RookMobility(sqs[i], all&^kings[1].Bitboard())
		case WhiteQueen:
			attack[0] |= QueenMobility(sqs[i], all&^kings[1].Bitboard())
		case WhiteKing:
			attack[0] |= KingMobility(sqs[i])
		case BlackKing:
			attack[1] |= KingMobility(sqs[i])
		}
	}

	if !s.isValid(sqs) {
		s.table[encode(sqs, 0)] = Result{Invalid, 0}
		s.table[encode(sqs, 1)] = Result{Invalid, 0}
		return
	}

	var mm [2]minimax
	for i := range s.pieces {
		toMove := 0
		if s.pieces[i].Color() == Black {
			toMove = 1
		}

		// Check whether this position is valid or known draw.
		if mm[toMove].Result.Outcome == Invalid {
			continue
		}
		if s.pieces[i].Figure() == King && attack[toMove].Has(kings[1-toMove]) {
			mm[toMove].Result = Result{Invalid, 0}
			continue
		}

		// Compute piece's mobility.
		var mob Bitboard
		switch s.pieces[i].Figure() {
		case Pawn:
			bb := sqs[i].Bitboard()
			mob = Forward(s.pieces[i].Color(), bb) &^ all
			mob |= Forward(s.pieces[i].Color(), East(bb)|West(bb)) & boards[1-toMove]
			if sqs[i].Rank() == 1 {
				mob |= North(North(bb)&^all) &^ all
			}
		case Rook:
			mob = RookMobility(sqs[i], all)
		case Queen:
			mob = QueenMobility(sqs[i], all)
		case King:
			mob = KingMobility(sqs[i]) &^ attack[1-toMove]
		}

		// Exclude own pieces for mobility.
		mob &^= boards[toMove]

		for mob != 0 {
			sq := mob.Pop()
			sqsnext := sqs
			sqsnext[i] = sq

			if s.pieces[i] == WhitePawn && sq.Rank() == 7 {
				// Promote white pawn.
				r := krk[encode(sqsnext, 1)]
				mm[toMove].update(Result{r.Outcome, 0})
				q := kqk[encode(sqsnext, 1)]
				mm[toMove].update(Result{q.Outcome, 0})
			} else {
				// Regular move.
				resultNext := s.table[encode(sqsnext, 1-toMove)]
				if s.pieces[i].Figure() == Pawn {
					resultNext.DTZ = 0
				}

				mm[toMove].update(resultNext)
			}

			if toMove == 1 && boards[0].Has(sq) {
				// TODO: handle all captures.
				mm[toMove].update(Result{Draw, 0})
			}
		}
	}

	// Detect mates and stalemates.
	for i := 0; i <= 1; i++ {
		if mm[i].NumMoves == 0 && mm[i].Result.Outcome != Invalid {
			if attack[1-i].Has(kings[i]) {
				mm[i].Result = Result{Lost, 0}
			} else {
				mm[i].Result = Result{Draw, 0}
			}
		}
	}

	// Update the result.
	modified := false
	for toMove := 0; toMove <= 1; toMove++ {
		ii := encode(sqs, toMove)
		if s.table[ii] != mm[toMove].Result {
			s.table[ii] = mm[toMove].Result
			modified = true
		}
	}

	// If the result was modified, enqueue the neighbors.
	if modified {
		for i := range s.pieces {
			var mob Bitboard
			switch s.pieces[i] {
			case WhitePawn:
				bb := sqs[i].Bitboard()
				mob = South(bb) &^ all &^ BbRank1
			case WhiteRook:
				mob = RookMobility(sqs[i], all) &^ all
			case WhiteQueen:
				mob = QueenMobility(sqs[i], all) &^ all
			case WhiteKing:
				mob = KingMobility(sqs[i]) &^ all &^ KingMobility(kings[1])
			case BlackKing:
				mob = KingMobility(sqs[i]) &^ all &^ KingMobility(kings[0])
			}

			for mob != 0 {
				sq := mob.Pop()
				sqs1 := sqs
				sqs1[i] = sq
				if ii := encode(sqs1, 0); !s.instack[ii] {
					s.stacknew = append(s.stacknew, sqs1)
					s.instack[ii] = true
				}
			}
		}
	}
}

func (s searcher) isValid(sqs squares) bool {
	for i := range s.pieces {
		if s.pieces[i] == NoPiece {
			break
		}
		if s.pieces[i].Figure() == Pawn && (sqs[i].Rank() == 0 || sqs[i].Rank() == 7) {
			return false
		}
		if s.pieces[i] == BlackKing && distance[sqs[0]][sqs[i]] <= 1 {
			return false
		}
		for j := range s.pieces[:] {
			if j > i && s.pieces[j] != NoPiece && sqs[i] == sqs[j] {
				return false
			}
		}
	}
	return true
}

func encode(sqs squares, toMove int) int {
	e := 0
	e = e*64 + int(sqs[7])
	e = e*64 + int(sqs[6])
	e = e*64 + int(sqs[5])
	e = e*64 + int(sqs[4])
	e = e*64 + int(sqs[3])
	e = e*64 + int(sqs[2])
	e = e*64 + int(sqs[1])
	e = e*64 + int(sqs[0])
	e = e*2 + toMove
	return e
}

func getTableSize(pis pieces) int {
	s := 2
	for _, p := range pis {
		if p != NoPiece {
			s *= 64
		}
	}
	return s
}

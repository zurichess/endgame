// Copyright 2014-2017 The Zurichess Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package endgame

import (
	. "bitbucket.org/zurichess/zurichess/board"
	"log"
)

// Size of krkr bitbase.
const krkrSize = 64 * 64 * 64 * 64
const krkrMaxMoves = 6

// WDL and DTM (DTZ?) bitbases.
var krkr [krkrSize]Result

func krkrEncode(wk, wr, bk, br Square) int {
	if wk.Rank() >= 4 {
		wk, wr = wk^0x38, wr^0x38
		bk, br = bk^0x38, br^0x38
	}
	if wk.File() >= 4 {
		wk, wr = wk^0x7, wr^0x7
		bk, br = bk^0x7, br^0x7
	}

	return (((int(wk)*64+int(wr))*64)+int(bk))*64 + int(br)
}

// krkrEasy solves trivial and cached krkr positions.
func krkrEasy(wk, wr, bk, br Square) Result {
	if distance[wk][bk] <= 1 || wk == wr || bk == br {
		// kings touch, or overlapping pieces.
		return Result{Invalid, 0}
	}
	if br == wk {
		// Black's rook captured white king.
		return Result{Invalid, 0}
	}
	if bk == wr || br == wr {
		// white rook captured, degenerate into krk.
		r := krk[encode(squares{bk, br, wk}, 1)]
		return Result{r.Outcome, 0}
	}

	ii := krkrEncode(wk, wr, bk, br)
	return krkr[ii]
}

// krkrBuildPosition attempts to improve current positions for white and black.
// returns the number of positions modified (at most 2).
func krkrBuildPosition(wk, wr, bk, br Square) int {
	if bk == wr || br == wr {
		return 0
	}

	modified := 0
	wkm := KingMobility(wk)
	wrm := RookMobility(wr, wk.Bitboard()|wr.Bitboard()|br.Bitboard())
	bkm := KingMobility(bk)
	brm := RookMobility(br, bk.Bitboard()|br.Bitboard()|wr.Bitboard())

	if wrm.Has(bk) { // Enemy king can be captured.
		r := Result{Invalid, 0}
		ii := krkrEncode(wk, wr, bk, br)
		if r != krkr[ii] {
			krkr[ii] = r
			modified++
		}
		return modified
	}

	if r := krkrEasy(wk, wr, bk, br); r.Outcome != Invalid {
		var mm minimax

		{ // king
			mob := wkm &^ (bkm | brm | wr.Bitboard())
			for bb := mob; bb != 0; {
				sq := bb.Pop()
				mm.update(krkrEasy(bk, br, sq, wr))
			}
		}

		{ // rook
			mob := wrm &^ (wk.Bitboard())
			for bb := mob; bb != 0; {
				sq := bb.Pop()
				mm.update(krkrEasy(bk, br, wk, sq))
			}
		}

		if mm.NumMoves == 0 {
			if brm.Has(wk) {
				r = Result{Lost, 0}
			} else {
				r = Result{Draw, 0}
			}
		}

		ii := krkrEncode(wk, wr, bk, br)
		if mm.Result != krkr[ii] {
			krkr[ii] = mm.Result
			modified++
		}
	}

	return modified
}

func krkrBuild() {
	for modified := -1; modified != 0; {
		modified = 0
		for wr := SquareMinValue; wr <= SquareMaxValue; wr++ {
			for wk := SquareMinValue; wk <= SquareMaxValue; wk++ {
				for br := SquareMinValue; br <= SquareMaxValue; br++ {
					for bk := SquareMinValue; bk <= SquareMaxValue; bk++ {
						modified += krkrBuildPosition(wk, wr, bk, br)
					}
				}
			}
		}
		log.Println("krkr: updated", modified, "positions")
	}
}

// KRKR evaluates the KRKR endgame.
func KRKR(pos *Position) Result {
	var wk, wr, bk, br Square

	if pos.Us() == White {
		wk = pos.ByPiece(White, King).AsSquare()
		wr = pos.ByPiece(White, Rook).AsSquare()
		bk = pos.ByPiece(Black, King).AsSquare()
		br = pos.ByPiece(Black, Rook).AsSquare()
	} else if pos.Us() == Black {
		wk = pos.ByPiece(Black, King).AsSquare()
		wr = pos.ByPiece(Black, Rook).AsSquare()
		bk = pos.ByPiece(White, King).AsSquare()
		br = pos.ByPiece(White, Rook).AsSquare()
	} else {
		panic("invalid color")
	}

	return krkrEasy(wk, wr, bk, br)
}

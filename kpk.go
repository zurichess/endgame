// Copyright 2014-2017 The Zurichess Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package endgame

import (
	. "bitbucket.org/zurichess/zurichess/board"
	"log"
)

var kpkWDL []Result

func kpkBuild() {
	log.Println("building kpk")
	pieces := pieces{WhiteKing, WhitePawn, BlackKing}
	kpkWDL = buildTable(pieces)
}

func KPK(pos *Position) Result {
	var wk, wp, bk Square
	var toMove int

	if pos.ByPiece(White, Pawn) != 0 {
		wk = pos.ByPiece(White, King).AsSquare().POV(White)
		wp = pos.ByPiece(White, Pawn).AsSquare().POV(White)
		bk = pos.ByPiece(Black, King).AsSquare().POV(White)
		if pos.Us() == White {
			toMove = 0
		} else {
			toMove = 1
		}
	} else if pos.ByPiece(Black, Pawn) != 0 {
		wk = pos.ByPiece(Black, King).AsSquare().POV(Black)
		wp = pos.ByPiece(Black, Pawn).AsSquare().POV(Black)
		bk = pos.ByPiece(White, King).AsSquare().POV(Black)
		if pos.Us() == Black {
			toMove = 0
		} else {
			toMove = 1
		}
	} else {
		panic("no pawns")
	}

	return kpkWDL[encode(squares{wk, wp, bk}, toMove)]
}

// Copyright 2014-2017 The Zurichess Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package endgame

import (
	. "bitbucket.org/zurichess/zurichess/board"
	"log"
	"testing"
)

type testExample struct {
	fen    string
	result string
	dtz    int16
}

var craftedTestData = []testExample{
	{"8/8/8/8/8/2K5/k7/4R3 w - - 23 13", "1-0", 3},
	{"8/8/8/8/2K1R3/8/2k5/8 w - - 15 9", "1-0", 11},
	{"4R3/8/8/3K4/8/3k4/8/8 b - - 6 4", "1-0", 20},
	{"2R5/5K2/8/8/8/8/8/3k4 w - - 1 2", "1-0", 25},
	{"2R5/5K2/8/8/8/8/3k4/8 b - - 0 1", "1-0", 26},
	{"2R5/5K2/8/8/8/3k4/8/8 w - - 1 2", "1-0", 25},

	// KK, KNK, KBK
	{"8/5k2/3N4/8/2K5/8/8/8 b - - 0 1", "1/2-1/2", 0},
	{"8/5k2/3B4/8/2K5/8/8/8 b - - 0 1", "1/2-1/2", 0},
	{"8/5k2/8/8/2K5/8/8/8 b - - 0 1", "1/2-1/2", 0},
	// KPK
	{"8/5P1k/8/5K2/8/8/8/8 w - - 0 1", "1-0", 1},
	{"7k/7P/7K/8/8/8/8/8 w - - 0 1", "1/2-1/2", 0},
	{"2k5/8/2P5/3K4/8/8/8/8 w - - 0 1", "1/2-1/2", 0},
	{"2k5/8/2P5/2K5/8/8/8/8 b - - 1 1", "1/2-1/2", 0},
	{"8/2k5/2P5/2K5/8/8/8/8 w - - 2 2", "1/2-1/2", 0},
	{"4k3/8/4P1K1/8/8/8/8/8 b - - 6 7", "1/2-1/2", 0},
	{"8/k7/1PK5/8/8/8/8/8 b - - 0 1", "1/2-1/2", 0},
	{"k7/1P6/2K5/8/8/8/8/8 b - - 0 2", "1/2-1/2", 0},
	{"k7/8/1PK5/8/8/8/8/8 w - - 1 2", "1/2-1/2", 0},
	{"k7/1P6/2K5/8/8/8/8/8 b - - 0 2", "1/2-1/2", 0},
	{"k7/2K5/1P6/8/8/8/8/8 b - - 2 2", "1/2-1/2", 0},
	{"8/8/8/4k2K/8/8/5P2/8 w - -", "1-0", 3},
	// KRK
	{"1R2k3/8/4K3/8/8/8/8/8 b - - 0 1", "1-0", 0},
	{"6k1/7R/8/7K/8/8/8/8 b - - 0 1", "1/2-1/2", 0},
	{"6R1/7k/8/8/3K4/8/8/8 b - - 0 1", "1/2-1/2", 0},
	{"7k/6R1/6K1/8/8/8/8/8 b - - 0 1", "1/2-1/2", 0},
	// Misc
	{"8/8/2R2k2/8/2K5/8/8/8 w - - 0 1", "(invalid)", 0},
	{"8/8/2Q2k2/8/2K5/8/8/8 w - - 0 1", "(invalid)", 0},
}

func resultToString(r Outcome, c Color) string {
	if r == Unknown {
		return "(unknown)"
	}
	if r == Invalid {
		return "(invalid)"
	}
	if r == Won && c == White || r == Lost && c == Black {
		return "1-0"
	}
	if r == Lost && c == White || r == Won && c == Black {
		return "0-1"
	}
	return "1/2-1/2"
}

func testEndgame(t *testing.T, testdata []testExample) {
	t.Helper()
	numBad := 0
	for i, want := range testdata {
		pos, err := PositionFromFEN(want.fen)
		if err != nil {
			t.Errorf("Failed to parse %s: %s", want.fen, err)
			continue
		}

		result := Evaluate(pos)
		got := resultToString(result.Outcome, pos.Us())

		if got != want.result || want.result != "1/2-1/2" && result.DTZ != want.dtz {
			numBad++
			if numBad <= 25 || want.dtz <= 24 {
				t.Errorf("#%d For %s got %s in %d, wanted %s in %d",
					i, want.fen, got, result.DTZ, want.result, want.dtz)
			}
		}
	}

	if numBad != 0 {
		t.Log("Got", numBad, "out of", len(testdata), "wrong")
	}
}

func TestKRK(t *testing.T) {
	testEndgame(t, krkTestData)
}

func TestKQK(t *testing.T) {
	testEndgame(t, kqkTestData)
}

func TestKPK(t *testing.T) {
	// kpkBuild()
	log.Println("kpk took", run(kpkBuild))
	testEndgame(t, kpkTestData)
}

func TestCrafted(t *testing.T) {
	testEndgame(t, craftedTestData)
}

/*
func TestKRKR(t *testing.T) {
	// testEndgame(t, krkrTestData)
}
*/

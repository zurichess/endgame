// Copyright 2014-2017 The Zurichess Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package endgame

import (
	. "bitbucket.org/zurichess/zurichess/board"
	"log"
)

// Size of krk bitbase.
const krkSize = 64 * 64 * 64 * 2

// WDL and DTM (DTZ?) bitbases.
var krk []Result

func krkBuild() {
	log.Println("building krk")
	pieces := pieces{WhiteKing, WhiteRook, BlackKing}
	krk = buildTable(pieces)
}

// KRK evaluates the KRK endgame.
func KRK(pos *Position) Result {
	var wk, wr, bk Square
	var toMove int

	if pos.ByPiece(pos.Us(), Rook) != 0 {
		wk = pos.ByPiece(pos.Us(), King).AsSquare()
		wr = pos.ByPiece(pos.Us(), Rook).AsSquare()
		bk = pos.ByPiece(pos.Them(), King).AsSquare()
		toMove = 0
	} else if pos.ByPiece(pos.Them(), Rook) != 0 {
		wk = pos.ByPiece(pos.Them(), King).AsSquare()
		wr = pos.ByPiece(pos.Them(), Rook).AsSquare()
		bk = pos.ByPiece(pos.Us(), King).AsSquare()
		toMove = 1
	} else {
		panic("no rooks")
	}

	return krk[encode(squares{wk, wr, bk}, toMove)]
}
